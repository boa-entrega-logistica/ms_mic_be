# Repositório para armazenamento do trabalho de conclusão do curso de Especialização em Arquitetura de Software Distribuído da PUC Minas.

Back-end desenvolvido em java 11 com spring boot 2.5.4


Execute o comando `docker-compose up` na pasta docker, na raiz do projeto para criar a base de dados no PostgreSQL.


Para acessar o contrato da API, basta acessar o localhost do swagger http://localhost:8080/swagger-ui/index.html#/




