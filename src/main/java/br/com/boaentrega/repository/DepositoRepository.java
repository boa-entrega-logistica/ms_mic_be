package br.com.boaentrega.repository;

import br.com.boaentrega.entity.Deposito;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DepositoRepository extends JpaRepository<Deposito, Long> {

    boolean existsByCnpj(String cnpj);

    Optional<Deposito> findByCnpj(String cnpj);
}
