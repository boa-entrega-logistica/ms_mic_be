package br.com.boaentrega.repository;


import br.com.boaentrega.entity.Rota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RotaRepository extends JpaRepository<Rota, Long> {

}
