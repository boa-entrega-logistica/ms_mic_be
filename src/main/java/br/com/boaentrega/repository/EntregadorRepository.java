package br.com.boaentrega.repository;


import br.com.boaentrega.entity.Cliente;
import br.com.boaentrega.entity.Entregador;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EntregadorRepository extends JpaRepository<Entregador, Long> {

    boolean existsByCpf(String cpf);

    Optional<Entregador> findByCpf(String cpf);

}
