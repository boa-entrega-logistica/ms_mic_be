package br.com.boaentrega.controller;

import br.com.boaentrega.dto.EntregadorDTO;
import br.com.boaentrega.mapper.EntregadorMapper;
import br.com.boaentrega.service.EntregadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/entregador")
public class EntregadorController {

    @Autowired
    private EntregadorService entregadorService;

    @Autowired
    private EntregadorMapper entregadorMapper;

    @PostMapping
    public ResponseEntity<EntregadorDTO> cadastrarEntregador(@RequestBody @Validated EntregadorDTO entregadorDTO,
                                                             UriComponentsBuilder uriBuilder) {
        var entregadorCadastrado = entregadorService.cadastrar(entregadorMapper.toEntregador(entregadorDTO));
        URI uri = uriBuilder.path("/").buildAndExpand(entregadorCadastrado.getId()).toUri();

        return ResponseEntity.created(uri).body(entregadorMapper.toEntregadorDTO(entregadorCadastrado));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletarEntregador(@PathVariable("id") Long id) {
        entregadorService.deletar(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/cpf/{cpf}")
    public ResponseEntity<EntregadorDTO> buscarEntregadorPorCpf(@PathVariable("cpf") String cpf) {
        return ResponseEntity.ok(entregadorMapper.toEntregadorDTO(entregadorService.buscarEntregadorPorCpf(cpf)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntregadorDTO> buscarEntregadorPorId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(entregadorMapper.toEntregadorDTO(entregadorService.buscarEntregadorPorId(id)));
    }

    @GetMapping
    public ResponseEntity<List<EntregadorDTO>> buscarTodosEntregadores() {
        return ResponseEntity.ok(entregadorService.buscarTodosEntregadores()
                .stream()
                .map(entregadorMapper::toEntregadorDTO)
                .collect(Collectors.toList()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EntregadorDTO> alterarEntregador(@PathVariable("id") Long id,
                                                           @RequestBody @Validated EntregadorDTO EntregadorDTO) {
        var entregadorAlterado = entregadorService.alterarEntrgador(id, entregadorMapper.toEntregador(EntregadorDTO));
        return ResponseEntity.ok(entregadorMapper.toEntregadorDTO(entregadorAlterado));
    }

}
