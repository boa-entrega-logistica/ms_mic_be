package br.com.boaentrega.controller;

import br.com.boaentrega.dto.DepositoDTO;
import br.com.boaentrega.mapper.DepositoMapper;
import br.com.boaentrega.service.DepositoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/deposito")
public class DepositoController {

    @Autowired
    private DepositoService depositoService;
    @Autowired
    private DepositoMapper depositoMapper;

    @PostMapping
    public ResponseEntity<DepositoDTO> cadastrarDeposito(@RequestBody @Validated DepositoDTO depositoDTO,
                                                         UriComponentsBuilder uriBuilder) {
        var depositoCadastrado = depositoService.cadastrar(depositoMapper.toDeposito(depositoDTO));
        URI uri = uriBuilder.path("/").buildAndExpand(depositoCadastrado.getId()).toUri();

        return ResponseEntity.created(uri).body(depositoMapper.toDepositoDTO(depositoCadastrado));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletarDeposito(@PathVariable("id") Long id) {
        depositoService.deletar(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/cnpj/{cnpj}")
    public ResponseEntity<DepositoDTO> buscarDepositoPorCnpj(@PathVariable("cnpj") String cnpj) {
        return ResponseEntity.ok(depositoMapper.toDepositoDTO(depositoService.buscarDepositoPorCnpj(cnpj)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DepositoDTO> buscarDepositoPorId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(depositoMapper.toDepositoDTO(depositoService.buscarDepositoPorId(id)));
    }

    @GetMapping
    public ResponseEntity<List<DepositoDTO>> buscarTodosDeposito() {
        return ResponseEntity.ok(depositoService.buscarTodosDepositos()
                .stream()
                .map(depositoMapper::toDepositoDTO)
                .collect(Collectors.toList()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<DepositoDTO> alterarDeposito(@PathVariable("id") Long id,
                                                       @RequestBody @Validated DepositoDTO DepositoDTO) {
        var depositoAlterado = depositoService.alterarDeposito(id, depositoMapper.toDeposito(DepositoDTO));
        return ResponseEntity.ok(depositoMapper.toDepositoDTO(depositoAlterado));
    }

}
