package br.com.boaentrega.controller;

import br.com.boaentrega.dto.ClienteDTO;
import br.com.boaentrega.mapper.ClienteMapper;
import br.com.boaentrega.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping
    public ResponseEntity<ClienteDTO> cadastrarCliente(@RequestBody @Validated ClienteDTO clienteDTO,
                                                       UriComponentsBuilder uriBuilder) {
        var clienteCadastrado = clienteService.cadastrar(clienteMapper.toCliente(clienteDTO));
        URI uri = uriBuilder.path("/").buildAndExpand(clienteCadastrado.getId()).toUri();

        return ResponseEntity.created(uri).body(clienteMapper.toClienteDTO(clienteCadastrado));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletarCliente(@PathVariable("id") Long id) {
        clienteService.deletar(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/cpf-cnpj/{cpfCnpj}")
    public ResponseEntity<ClienteDTO> buscarClientePorCpfCnpj(@PathVariable("cpfCnpj") String cpfCnpj) {
        return ResponseEntity.ok(clienteMapper.toClienteDTO(clienteService.buscarClientePorCpfCnpj(cpfCnpj)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClienteDTO> buscarClientePorId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(clienteMapper.toClienteDTO(clienteService.buscarClientePorId(id)));
    }

    @GetMapping
    public ResponseEntity<List<ClienteDTO>> buscarTodosClientes() {
        return ResponseEntity.ok(clienteService.buscarTodosClientes()
                .stream()
                .map(clienteMapper::toClienteDTO)
                .collect(Collectors.toList()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClienteDTO> alterarCliente(@PathVariable("id") Long id,
                                                     @RequestBody @Validated ClienteDTO clienteDTO) {
        var clienteAlterado = clienteService.alterarCliente(id, clienteMapper.toCliente(clienteDTO));
        return ResponseEntity.ok(clienteMapper.toClienteDTO(clienteAlterado));
    }

}
