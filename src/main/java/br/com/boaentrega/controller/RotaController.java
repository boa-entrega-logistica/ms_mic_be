package br.com.boaentrega.controller;

import br.com.boaentrega.dto.RotaDTO;
import br.com.boaentrega.mapper.RotaMapper;
import br.com.boaentrega.service.RotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rota")
public class RotaController {

    @Autowired
    private RotaService rotaService;

    @Autowired
    private RotaMapper rotaMapper;


    @GetMapping("/{id}")
    public ResponseEntity<RotaDTO> buscarRotaPorId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(rotaMapper.toRotaDTO(rotaService.buscarRotaPorId(id)));
    }

    @GetMapping
    public ResponseEntity<List<RotaDTO>> buscarTodasRotas() {
        return ResponseEntity.ok(rotaService.buscarTodasRotas()
                .stream()
                .map(rotaMapper::toRotaDTO)
                .collect(Collectors.toList()));
    }

    @PostMapping
    public ResponseEntity<RotaDTO> cadastrarRota(@RequestParam Long idEntregador,
                                                 @RequestParam Long idDeposito,
                                                 @RequestParam Long idCliente) {
        var rotaCadastrada = rotaService.cadastrarRota(idEntregador, idDeposito, idCliente);
        return ResponseEntity.ok(rotaMapper.toRotaDTO(rotaCadastrada));
    }

    @PutMapping("/{id}")
    public ResponseEntity<RotaDTO> alterarRota(@PathVariable("id") Long idRota,
                                               @RequestParam Long idEntregador,
                                               @RequestParam Long idDeposito,
                                               @RequestParam Long idCliente) {
        var rotaAlterada = rotaService.alterarRota(idRota, idEntregador, idDeposito, idCliente);
        return ResponseEntity.ok(rotaMapper.toRotaDTO(rotaAlterada));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RotaDTO> deletarRota(@PathVariable("id") Long id) {
        rotaService.deletarRota(id);
        return ResponseEntity.noContent().build();
    }

}
