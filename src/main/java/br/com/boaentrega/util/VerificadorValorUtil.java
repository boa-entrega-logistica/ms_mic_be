package br.com.boaentrega.util;

import static br.com.boaentrega.constant.MensagemConstant.MENSAGEM_ERRO_VALOR_NULO;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

public interface VerificadorValorUtil {

    static <T> void verificaEstaNulo(T obj, String propriedade) {
        requireNonNull(obj, format(MENSAGEM_ERRO_VALOR_NULO, propriedade));
    }

}