package br.com.boaentrega.service;

import br.com.boaentrega.entity.Cliente;
import br.com.boaentrega.exception.JaExisteRegistroException;
import br.com.boaentrega.exception.NaoEncontrado;
import br.com.boaentrega.mask.Mask;
import br.com.boaentrega.repository.ClienteRepository;
import br.com.boaentrega.repository.EnderecoRepository;
import br.com.boaentrega.util.VerificadorValorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Transactional
    public Cliente cadastrar(Cliente cliente) {
        validarCPF(cliente);
        var novoCliente = clienteRepository.save(cliente);
        return novoCliente;
    }

    @Transactional
    public void deletar(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        clienteRepository.deleteById(id);

    }

    @Transactional(readOnly = true)
    public Cliente buscarClientePorCpfCnpj(String cpfCnpj) {
        VerificadorValorUtil.verificaEstaNulo(cpfCnpj, "CPFCNPJ");
        return clienteRepository.findByCpfCnpj(Mask.retirarMascara(cpfCnpj))
                .orElseThrow(() -> new NaoEncontrado("Cliente não encontrado"));
    }

    @Transactional(readOnly = true)
    public Cliente buscarClientePorId(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        return clienteRepository.findById(id)
                .orElseThrow(() -> new NaoEncontrado("Cliente não encontrado"));
    }

    @Transactional(readOnly = true)
    public List<Cliente> buscarTodosClientes() {
        return clienteRepository.findAll();
    }

    @Transactional
    public Cliente alterarCliente(Long id, Cliente cliente) {
        VerificadorValorUtil.verificaEstaNulo(cliente, "Parametro Cliente");
        VerificadorValorUtil.verificaEstaNulo(id, "ID");

        var clienteCadastrado = buscarClientePorId(id);
        var enderecoCadastrado = enderecoRepository.findById(clienteCadastrado.getEndereco().getId());

        clienteCadastrado.setCpfCnpj(cliente.getCpfCnpj());
        clienteCadastrado.setNome(cliente.getNome());
        clienteCadastrado.setTelefone(cliente.getTelefone());

        if (enderecoCadastrado.isPresent()) {
            enderecoCadastrado.get().setCep(cliente.getEndereco().getCep());
            enderecoCadastrado.get().setBairro(cliente.getEndereco().getBairro());
            enderecoCadastrado.get().setEstado(cliente.getEndereco().getEstado());
            enderecoCadastrado.get().setMunicipio(cliente.getEndereco().getMunicipio());
            enderecoCadastrado.get().setNumero(cliente.getEndereco().getNumero());
            enderecoCadastrado.get().setLogradouro(cliente.getEndereco().getLogradouro());
            enderecoCadastrado.get().setLatitude(cliente.getEndereco().getLatitude());
            enderecoCadastrado.get().setLongitude(cliente.getEndereco().getLongitude());
        }

        return clienteCadastrado;

    }

    private void validarCPF(Cliente cliente) {
        boolean exiteClienteComMesmoCPFCNPJ = clienteRepository.existsByCpfCnpj(cliente.getCpfCnpj());
        if (exiteClienteComMesmoCPFCNPJ) {
            throw new JaExisteRegistroException("Já possui um cliente com o mesmo CPF/CNPJ");
        }
    }


}
