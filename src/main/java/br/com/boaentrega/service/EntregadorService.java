package br.com.boaentrega.service;

import br.com.boaentrega.entity.Entregador;
import br.com.boaentrega.exception.JaExisteRegistroException;
import br.com.boaentrega.exception.NaoEncontrado;
import br.com.boaentrega.mask.Mask;
import br.com.boaentrega.repository.EntregadorRepository;
import br.com.boaentrega.util.VerificadorValorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class EntregadorService {

    @Autowired
    private EntregadorRepository entregadorRepository;

    @Transactional
    public Entregador cadastrar(Entregador entregador) {
        validarCPF(entregador);
        var novoEntregador = entregadorRepository.save(entregador);
        return novoEntregador;
    }

    @Transactional
    public void deletar(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        entregadorRepository.deleteById(id);

    }

    @Transactional(readOnly = true)
    public Entregador buscarEntregadorPorCpf(String cpf) {
        VerificadorValorUtil.verificaEstaNulo(cpf, "CPF");
        return entregadorRepository.findByCpf(Mask.retirarMascara(cpf))
                .orElseThrow(() -> new NaoEncontrado("Entregador não encontrado"));
    }

    @Transactional(readOnly = true)
    public Entregador buscarEntregadorPorId(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        return entregadorRepository.findById(id)
                .orElseThrow(() -> new NaoEncontrado("Entregador não encontrado"));
    }

    @Transactional(readOnly = true)
    public List<Entregador> buscarTodosEntregadores() {
        return entregadorRepository.findAll();
    }

    @Transactional
    public Entregador alterarEntrgador(Long id, Entregador entregador) {
        VerificadorValorUtil.verificaEstaNulo(entregador, "Parametro Entregador");
        VerificadorValorUtil.verificaEstaNulo(id, "ID");

        var entregadorCadastrado = buscarEntregadorPorId(id);

        entregadorCadastrado.setCpf(entregador.getCpf());
        entregadorCadastrado.setNome(entregador.getNome());
        entregadorCadastrado.setTelefone(entregador.getTelefone());
        entregadorCadastrado.setLatitudeAgora(entregador.getLatitudeAgora());
        entregadorCadastrado.setLongitudeAgora(entregador.getLongitudeAgora());
        entregadorCadastrado.setPlacaVeiculo(entregador.getPlacaVeiculo());

        return entregadorCadastrado;

    }

    private void validarCPF(Entregador entregador) {
        boolean exiteEntregadorComMesmoCPFCNPJ = entregadorRepository.existsByCpf(entregador.getCpf());
        if (exiteEntregadorComMesmoCPFCNPJ) {
            throw new JaExisteRegistroException("Já possui um entregador com o mesmo CPF");
        }
    }


}
