package br.com.boaentrega.service;

import br.com.boaentrega.entity.Rota;
import br.com.boaentrega.exception.NaoEncontrado;
import br.com.boaentrega.repository.ClienteRepository;
import br.com.boaentrega.repository.DepositoRepository;
import br.com.boaentrega.repository.RotaRepository;
import br.com.boaentrega.util.VerificadorValorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RotaService {

    @Autowired
    private RotaRepository rotaRepository;

    @Autowired
    private EntregadorService entregadorService;

    @Autowired
    private DepositoService depositoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private DepositoRepository depositoRepository;


    @Transactional(readOnly = true)
    public Rota buscarRotaPorId(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        return rotaRepository.findById(id)
                .orElseThrow(() -> new NaoEncontrado("Rota não encontrado"));
    }

    @Transactional(readOnly = true)
    public List<Rota> buscarTodasRotas() {
        return rotaRepository.findAll();
    }

    @Transactional
    public Rota cadastrarRota(Long idEntregador, Long idDeposito, Long idCliente) {
        Rota rota = new Rota();
        var entregadorCadastrado = entregadorService.buscarEntregadorPorId(idEntregador);
        var depositoCadastrado = depositoService.buscarDepositoPorId(idDeposito);
        var clienteCadastrado = clienteService.buscarClientePorId(idCliente);

        rota.setEntregador(entregadorCadastrado);
        rota.setDeposito(depositoCadastrado);
        rota.setCliente(clienteCadastrado);

        var rotaCadastrada = rotaRepository.save(rota);

        return rotaCadastrada;

    }

    @Transactional
    public Rota alterarRota(Long idRota, Long idEntregador, Long idDeposito, Long idCliente) {
        var rotaCadastrada = buscarRotaPorId(idRota);
        var entregadorCadastrado = entregadorService.buscarEntregadorPorId(idEntregador);
        var depositoCadastrado = depositoService.buscarDepositoPorId(idDeposito);
        var clienteCadastrado = clienteService.buscarClientePorId(idCliente);

        rotaCadastrada.setEntregador(entregadorCadastrado);
        rotaCadastrada.setDeposito(depositoCadastrado);
        rotaCadastrada.setCliente(clienteCadastrado);

        return rotaCadastrada;

    }

    @Transactional
    public void deletarRota(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        rotaRepository.deleteById(id);

    }
}
