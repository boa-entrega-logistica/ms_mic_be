package br.com.boaentrega.service;

import br.com.boaentrega.entity.Deposito;
import br.com.boaentrega.exception.JaExisteRegistroException;
import br.com.boaentrega.exception.NaoEncontrado;
import br.com.boaentrega.mask.Mask;
import br.com.boaentrega.repository.DepositoRepository;
import br.com.boaentrega.repository.EnderecoRepository;
import br.com.boaentrega.util.VerificadorValorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DepositoService {

    @Autowired
    private DepositoRepository depositoRepository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Transactional
    public Deposito cadastrar(Deposito deposito) {
        validarCNPJ(deposito);
        var novoDeposito = depositoRepository.save(deposito);
        return novoDeposito;
    }

    @Transactional
    public void deletar(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        depositoRepository.deleteById(id);

    }

    @Transactional(readOnly = true)
    public Deposito buscarDepositoPorCnpj(String cnpj) {
        VerificadorValorUtil.verificaEstaNulo(cnpj, "CNPJ");
        return depositoRepository.findByCnpj(Mask.retirarMascara(cnpj))
                .orElseThrow(() -> new NaoEncontrado("Deposito não encontrado"));
    }

    @Transactional(readOnly = true)
    public Deposito buscarDepositoPorId(Long id) {
        VerificadorValorUtil.verificaEstaNulo(id, "ID");
        return depositoRepository.findById(id)
                .orElseThrow(() -> new NaoEncontrado("Deposito não encontrado"));
    }

    @Transactional(readOnly = true)
    public List<Deposito> buscarTodosDepositos() {
        return depositoRepository.findAll();
    }

    @Transactional
    public Deposito alterarDeposito(Long id, Deposito deposito) {
        VerificadorValorUtil.verificaEstaNulo(deposito, "Parametro Deposito");
        VerificadorValorUtil.verificaEstaNulo(id, "ID");

        var depositoCadastrado = buscarDepositoPorId(id);
        var enderecoCadastrado = enderecoRepository.findById(depositoCadastrado.getEndereco().getId());

        depositoCadastrado.setNome(deposito.getNome());
        depositoCadastrado.setTelefone(deposito.getTelefone());
        depositoCadastrado.setCnpj(deposito.getCnpj());

        if (enderecoCadastrado.isPresent()) {
            enderecoCadastrado.get().setCep(deposito.getEndereco().getCep());
            enderecoCadastrado.get().setBairro(deposito.getEndereco().getBairro());
            enderecoCadastrado.get().setEstado(deposito.getEndereco().getEstado());
            enderecoCadastrado.get().setMunicipio(deposito.getEndereco().getMunicipio());
            enderecoCadastrado.get().setNumero(deposito.getEndereco().getNumero());
            enderecoCadastrado.get().setLogradouro(deposito.getEndereco().getLogradouro());
            enderecoCadastrado.get().setLatitude(deposito.getEndereco().getLatitude());
            enderecoCadastrado.get().setLongitude(deposito.getEndereco().getLongitude());
        }

        return depositoCadastrado;

    }

    private void validarCNPJ(Deposito deposito) {
        boolean exiteDepositoComMesmoCNPJ = depositoRepository.existsByCnpj(deposito.getCnpj());
        if (exiteDepositoComMesmoCNPJ) {
            throw new JaExisteRegistroException("Já possui um deposito com o mesmo CNPJ");
        }
    }
}
