package br.com.boaentrega.exception;

public class JaExisteRegistroException extends RuntimeException{
    public JaExisteRegistroException(String message){
        super(message);
    }
}

