package br.com.boaentrega.exception.controller;

import br.com.boaentrega.exception.JaExisteRegistroException;
import br.com.boaentrega.exception.NaoEncontrado;
import br.com.boaentrega.exception.ServiceException;
import br.com.boaentrega.exception.ValorPropriedadeInvalidoException;
import br.com.boaentrega.exception.model.ErroDTO;
import br.com.boaentrega.exception.model.ErroFormularioDTO;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class HandleExceptionController {
    private static final Logger log = LoggerFactory.getLogger(HandleExceptionController.class);

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public List<ErroFormularioDTO> handle(MethodArgumentNotValidException exception) {
        return exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> new ErroFormularioDTO(error.getField(), error.getDefaultMessage(),
                        HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase()))
                .collect(Collectors.toList());

    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = JsonParseException.class)
    public ErroDTO handleJsonParse(JsonMappingException exception) {
        log.error(exception.getMessage());
        return new ErroDTO("Não foi possivel criar objeto com Json enviado. Certifique-se que a formatação do Json esta correta",
                HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = JsonMappingException.class)
    public ErroDTO handleJsonMapping(JsonMappingException exception) {
        log.error(exception.getMessage());
        return new ErroDTO(exception.getMessage() + " " + exception.getLocalizedMessage() + " " + exception.getPathReference(),
                HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NullPointerException.class)
    public ErroDTO handleNulPointer(NullPointerException exception) {
        log.error(exception.getLocalizedMessage());
        return new ErroDTO(exception.getMessage(), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValorPropriedadeInvalidoException.class)
    public ErroDTO handleValorInvalido(ValorPropriedadeInvalidoException exception) {
        log.error(exception.getMessage());
        return new ErroDTO(exception.getMessage(), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }


    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public ErroDTO handleBind(BindException exception) {
        log.error(exception.getMessage());
        return new ErroDTO(exception.getMessage(), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DateTimeParseException.class)
    public ErroDTO handleBind(DateTimeParseException exception) {
        log.error(exception.getMessage());
        return new ErroDTO("A data " + exception.getParsedString() + " está no formato errado",
                HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceException.class)
    public ErroDTO handleService(ServiceException exception) {
        log.error(exception.getMessage());
        return new ErroDTO(exception.getMessage(), HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase());
    }


    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ErroDTO handleBind(DataIntegrityViolationException exception) {
        log.error(exception.getMessage());
        return new ErroDTO(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }

    @ResponseStatus(code = HttpStatus.CONFLICT)
    @ExceptionHandler(JaExisteRegistroException.class)
    public ErroDTO handleBind(JaExisteRegistroException exception) {
        log.error(exception.getMessage());
        return new ErroDTO(exception.getMessage(), HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT.getReasonPhrase());
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NaoEncontrado.class)
    public ErroDTO handleBind(NaoEncontrado exception) {
        log.error(exception.getMessage());
        return new ErroDTO(exception.getMessage(), HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase());
    }

}