package br.com.boaentrega.exception;

public class ValorPropriedadeInvalidoException extends RuntimeException{

    public ValorPropriedadeInvalidoException(String message){
        super(message);
    }
}
