package br.com.boaentrega.exception.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class ErroDTO {
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime dataHora = LocalDateTime.now();
    private String mensagem;
    private Integer codigo;
    private String status;


    public ErroDTO(String mensagem, Integer codigo, String status){
        this.mensagem = mensagem;
        this.codigo = codigo;
        this.status = status;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public String getMensagem() {
        return mensagem;
    }

    public String getStatus() {
        return status;
    }

    public Integer getCodigo() {
        return codigo;
    }
}