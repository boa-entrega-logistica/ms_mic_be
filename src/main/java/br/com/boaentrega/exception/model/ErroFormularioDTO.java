package br.com.boaentrega.exception.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class ErroFormularioDTO {

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime dataHora = LocalDateTime.now();
    private String campo;
    private String erro;
    private Integer codigo;
    private String status;

    public ErroFormularioDTO(String campo, String erro, Integer codigo, String status) {
        this.campo = campo;
        this.erro = erro;
        this.codigo = codigo;
        this.status = status;
    }

    public String getCampo() {
        return campo;
    }

    public String getErro() {
        return erro;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public String getStatus() {
        return status;
    }

    public Integer getCodigo() {
        return codigo;
    }


}