package br.com.boaentrega.exception;

public class NaoEncontrado extends RuntimeException{
    public NaoEncontrado(String message){
        super(message);
    }
}
