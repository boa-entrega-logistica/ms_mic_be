package br.com.boaentrega.exception;

public class DataAcessException extends RuntimeException {
    public DataAcessException(String message) {
        super(message);
    }
}
