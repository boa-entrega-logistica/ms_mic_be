package br.com.boaentrega.constant;

public class RegraConstant {

    public static final String REGRA_TELEFONE = "\\([1-9]{2}\\) (?:[2-8]|9[1-9])[0-9]{3}\\-[0-9]{4}$";
    public static final String REGRA_CPF_CNPJ = "(^\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$)|(^\\d{2}\\.\\d{3}\\.\\d{3}\\/\\d{4}\\-\\d{2}$)";
    public static final String REGRA_CEP = "[0-9]{5}-[0-9]{3}";

    private RegraConstant(){}
}
