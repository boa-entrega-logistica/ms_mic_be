package br.com.boaentrega.constant;

public class MensagemConstant {

    public static final String MENSAGEM_ERRO_VALOR_NULO = "O valor do(a) %s não pode ser nulo";
    public static final String MENSAGEM_ERRO_VALOR_VAZIO = "O valor do(a) %s não pode ser vazio";

    private MensagemConstant() {
    }
}
