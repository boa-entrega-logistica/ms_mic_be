package br.com.boaentrega.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ClienteDTO {

    private Long id;

    @NotBlank
    private String nome;

    @NotBlank
    private String cpfCnpj;

    @NotBlank
    private String telefone;

    private EnderecoDTO endereco;

}
