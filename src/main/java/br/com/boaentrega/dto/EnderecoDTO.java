package br.com.boaentrega.dto;

import br.com.boaentrega.constant.RegraConstant;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class EnderecoDTO {

    private Long id;

    private String logradouro;

    private Integer numero;

    @NotBlank
    private String bairro;

    @NotBlank
    private String municipio;

    @NotBlank
    private String estado;

    @NotBlank
    private String latitude;

    @NotBlank
    private String longitude;

    @NotBlank
    @Pattern(regexp = RegraConstant.REGRA_CEP, message = "CEP está no formato incorreto")
    private String cep;

}
