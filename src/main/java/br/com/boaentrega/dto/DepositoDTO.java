package br.com.boaentrega.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class DepositoDTO {

    private Long id;

    @NotBlank
    private String nome;

    @NotBlank
    @CNPJ
    private String cnpj;

    @NotBlank
    private String telefone;

    private EnderecoDTO endereco;


}
