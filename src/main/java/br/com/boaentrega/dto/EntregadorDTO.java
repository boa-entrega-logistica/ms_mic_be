package br.com.boaentrega.dto;

import br.com.boaentrega.constant.RegraConstant;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;


@Getter
@Setter
public class EntregadorDTO {

    private Long id;

    @NotBlank
    private String nome;

    @NotBlank
    @CPF
    private String cpf;

    @NotBlank
    private String telefone;

    @NotBlank
    private String placaVeiculo;

    @NotBlank
    private String latitudeAgora;

    @NotBlank
    private String longitudeAgora;

    private RotaDTO rota;

    private List<DepositoDTO> depositos;

}
