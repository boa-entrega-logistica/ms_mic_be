package br.com.boaentrega.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RotaDTO {

    private Long id;

    private ClienteDTO cliente;

    private DepositoDTO deposito;

    private EntregadorDTO entregador;

}
