package br.com.boaentrega.config;

import br.com.boaentrega.dto.ClienteDTO;
import br.com.boaentrega.dto.DepositoDTO;
import br.com.boaentrega.dto.EntregadorDTO;
import br.com.boaentrega.dto.RotaDTO;
import br.com.boaentrega.entity.Cliente;
import br.com.boaentrega.entity.Deposito;
import br.com.boaentrega.entity.Entregador;
import br.com.boaentrega.entity.Rota;
import br.com.boaentrega.mask.Mask;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

    private Converter<String, String> semMascara = ctx -> Mask.retirarMascara(ctx.getSource());
    private org.modelmapper.Converter<String, String> mascaraTelefone = ctx -> Mask.mascaraTelefone(ctx.getSource());
    private Converter<String, String> mascaraCPFECNPJ = ctx -> Mask.mascaraCPFECNPJ(ctx.getSource());
    private Converter<String, String> mascaraCEP = ctx -> Mask.mascaraCEP(ctx.getSource());

    @Bean
    public ModelMapper modelMapper() {
        var modelMapper = new ModelMapper();

        this.configurarCliente(modelMapper);
        this.configurarClienteDTO(modelMapper);

        this.configurarDeposito(modelMapper);
        this.configurarDepositoDTO(modelMapper);

        this.configurarEntregador(modelMapper);
        this.configurarEntregadorDTO(modelMapper);

        this.configurarRota(modelMapper);
        this.configurarRotaDTO(modelMapper);
        return modelMapper;
    }

    private void configurarCliente(ModelMapper modelMapper) {
        modelMapper.createTypeMap(ClienteDTO.class, Cliente.class)
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .map(ClienteDTO::getCpfCnpj, Cliente::setCpfCnpj)
                )
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .map(ClienteDTO::getTelefone, Cliente::setTelefone)
                )
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .<String>map(clienteDTO -> clienteDTO.getEndereco().getCep(),
                                (cliente, cep) -> cliente.getEndereco().setCep(cep))
                );
    }

    private void configurarClienteDTO(ModelMapper modelMapper) {
        modelMapper.createTypeMap(Cliente.class, ClienteDTO.class)
                .addMappings(mapper -> mapper.using(this.mascaraCPFECNPJ)
                        .map(Cliente::getCpfCnpj, ClienteDTO::setCpfCnpj)
                )
                .addMappings(mapper -> mapper.using(this.mascaraTelefone)
                        .map(Cliente::getTelefone, ClienteDTO::setTelefone)
                )
                .addMappings(mapper -> mapper.using(this.mascaraCEP)
                        .<String>map(cliente -> cliente.getEndereco().getCep(),
                                (clienteDTO, cep) -> clienteDTO.getEndereco().setCep(cep))
                );
    }

    private void configurarDeposito(ModelMapper modelMapper) {
        modelMapper.createTypeMap(DepositoDTO.class, Deposito.class)
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .map(DepositoDTO::getCnpj, Deposito::setCnpj)
                )
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .map(DepositoDTO::getTelefone, Deposito::setTelefone)
                )
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .<String>map(depositoDTO -> depositoDTO.getEndereco().getCep(),
                                (deposito, cep) -> deposito.getEndereco().setCep(cep))
                );
    }

    private void configurarDepositoDTO(ModelMapper modelMapper) {
        modelMapper.createTypeMap(Deposito.class, DepositoDTO.class)
                .addMappings(mapper -> mapper.using(this.mascaraCPFECNPJ)
                        .map(Deposito::getCnpj, DepositoDTO::setCnpj)
                )
                .addMappings(mapper -> mapper.using(this.mascaraTelefone)
                        .map(Deposito::getTelefone, DepositoDTO::setTelefone)
                )
                .addMappings(mapper -> mapper.using(this.mascaraCEP)
                        .<String>map(deposito -> deposito.getEndereco().getCep(),
                                (depositoDTO, cep) -> depositoDTO.getEndereco().setCep(cep))
                );
    }

    private void configurarEntregador(ModelMapper modelMapper) {
        modelMapper.createTypeMap(EntregadorDTO.class, Entregador.class)
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .map(EntregadorDTO::getCpf, Entregador::setCpf)
                )
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .map(EntregadorDTO::getTelefone, Entregador::setTelefone)
                );
    }

    private void configurarEntregadorDTO(ModelMapper modelMapper) {
        modelMapper.createTypeMap(Entregador.class, EntregadorDTO.class)
                .addMappings(mapper -> mapper.using(this.mascaraCPFECNPJ)
                        .map(Entregador::getCpf, EntregadorDTO::setCpf)
                )
                .addMappings(mapper -> mapper.using(this.mascaraTelefone)
                        .map(Entregador::getTelefone, EntregadorDTO::setTelefone)
                );
    }

    private void configurarRota(ModelMapper modelMapper) {
        modelMapper.createTypeMap(RotaDTO.class, Rota.class)
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .<String>map(rotaDTO -> rotaDTO.getDeposito().getEndereco().getCep(),
                                (rota, cep) -> rota.getDeposito().getEndereco().setCep(cep))
                )
                .addMappings(mapper -> mapper.using(this.semMascara)
                        .<String>map(rotaDTO -> rotaDTO.getCliente().getEndereco().getCep(),
                                (rota, cep) -> rota.getCliente().getEndereco().setCep(cep))
                );
    }

    private void configurarRotaDTO(ModelMapper modelMapper) {
        modelMapper.createTypeMap(Rota.class, RotaDTO.class)
                .addMappings(mapper -> mapper.using(this.mascaraCEP)
                        .<String>map(rota -> rota.getDeposito().getEndereco().getCep(),
                                (rotaDTO, cep) -> rotaDTO.getDeposito().getEndereco().setCep(cep))
                )
                .addMappings(mapper -> mapper.using(this.mascaraCEP)
                        .<String>map(rota -> rota.getCliente().getEndereco().getCep(),
                                (rotaDTO, cep) -> rotaDTO.getCliente().getEndereco().setCep(cep))
                );
    }


}
