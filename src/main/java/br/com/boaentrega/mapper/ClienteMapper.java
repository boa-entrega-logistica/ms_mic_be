package br.com.boaentrega.mapper;

import br.com.boaentrega.dto.ClienteDTO;
import br.com.boaentrega.entity.Cliente;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    @Autowired
    private ModelMapper modelMapper;

    public Cliente toCliente(ClienteDTO clienteDTO) {
        return this.modelMapper.map(clienteDTO, Cliente.class);
    }

    public ClienteDTO toClienteDTO(Cliente cliente) {
        return this.modelMapper.map(cliente, ClienteDTO.class);
    }

}
