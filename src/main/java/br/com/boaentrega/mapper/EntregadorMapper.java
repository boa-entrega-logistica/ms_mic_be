package br.com.boaentrega.mapper;

import br.com.boaentrega.dto.EntregadorDTO;
import br.com.boaentrega.entity.Entregador;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EntregadorMapper {

    @Autowired
    private ModelMapper modelMapper;

    public Entregador toEntregador(EntregadorDTO entregadorDTO) {
        return this.modelMapper.map(entregadorDTO, Entregador.class);
    }

    public EntregadorDTO toEntregadorDTO(Entregador entregador) {
        return this.modelMapper.map(entregador, EntregadorDTO.class);
    }

}
