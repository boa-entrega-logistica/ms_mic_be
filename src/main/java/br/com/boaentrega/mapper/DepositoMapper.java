package br.com.boaentrega.mapper;

import br.com.boaentrega.dto.DepositoDTO;
import br.com.boaentrega.entity.Deposito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DepositoMapper {

    @Autowired
    private ModelMapper modelMapper;

    public Deposito toDeposito(DepositoDTO depositoDTO) {
        return this.modelMapper.map(depositoDTO, Deposito.class);
    }

    public DepositoDTO toDepositoDTO(Deposito deposito) {
        return this.modelMapper.map(deposito, DepositoDTO.class);
    }

}
