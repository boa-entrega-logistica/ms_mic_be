package br.com.boaentrega.mapper;

import br.com.boaentrega.dto.RotaDTO;
import br.com.boaentrega.entity.Rota;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RotaMapper {

    @Autowired
    private ModelMapper modelMapper;

    public Rota toRota(RotaDTO rotaDTO) {
        return this.modelMapper.map(rotaDTO, Rota.class);
    }

    public RotaDTO toRotaDTO(Rota rota) {
        return this.modelMapper.map(rota, RotaDTO.class);
    }

}
