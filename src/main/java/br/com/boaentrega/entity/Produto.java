package br.com.boaentrega.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "produto")
public class Produto implements Serializable {

    private static final long serialVersionUID = 4944628905568041638L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String descProduto;

    private Integer qtdGlobal;

    private Double valorProduto;

    private Double largura;

    private Double altura;

    private Double profundidade;

    private Double peso;

}
