package br.com.boaentrega.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "entregador")
public class Entregador implements Serializable {

    private static final long serialVersionUID = -5287021707584883773L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    private String cpf;

    private String telefone;

    private String placaVeiculo;

    private String latitudeAgora;

    private String longitudeAgora;

//    @OneToOne(mappedBy = "entregador", fetch = FetchType.LAZY)
//    private Rota rota;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Deposito> depositos;

}
