package br.com.boaentrega.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "rota")
public class Rota implements Serializable {

    private static final long serialVersionUID = 3697339940566009200L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_deposito")
    private Deposito deposito;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_entregador")
    private Entregador entregador;


}
