package br.com.boaentrega.mask;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;

public class Mask {

    private static final String PADRAO_CPF = "(\\d{3})(\\d{3})(\\d{3})(\\d{2})";
    private static final String PADRAO_CNPJ = "(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})";
    private static final String PADRAO_CEP = "(\\d{5})(\\d{3})";
    private static final String PADRAO_TELEFONE = "(\\d{2})(\\d{4})(\\d{4})";
    private static final String PADRAO_CELULAR = "(\\d{2})(\\d{5})(\\d{4})";
    private static Pattern pattern;
    private static Matcher matcher;

    private Mask() {
    }

    public static String mascaraCEP(String cep) {
        if (isNull(cep)) {
            return null;
        }

        pattern = Pattern.compile(PADRAO_CEP);
        matcher = pattern.matcher(cep);

        if (matcher.matches()) cep = matcher.replaceAll("$1-$2");

        return cep;
    }

    public static String mascaraCPFECNPJ(String cpf) {
        if (isNull(cpf)) {
            return null;
        }

        if (cpf.length() == 11) {

            pattern = Pattern.compile(PADRAO_CPF);
            matcher = pattern.matcher(cpf);

            if (matcher.matches()) cpf = matcher.replaceAll("$1.$2.$3-$4");

            return cpf;
        } else {

            pattern = Pattern.compile(PADRAO_CNPJ);
            matcher = pattern.matcher(cpf);

            if (matcher.matches()) cpf = matcher.replaceAll("$1.$2.$3/$4-$5");

            return cpf;

        }
    }

    public static String mascaraTelefone(String telefone) {
        if (isNull(telefone)) {
            return null;
        }

        if (telefone.length() == 11) {
            return mascaraCelular(telefone);
        }

        pattern = Pattern.compile(PADRAO_TELEFONE);
        matcher = pattern.matcher(telefone);
        if (matcher.matches()) telefone = matcher.replaceAll("($1) $2-$3");

        return telefone;
    }

    public static String mascaraCelular(String celular) {
        if (isNull(celular)) {
            return null;
        }

        pattern = Pattern.compile(PADRAO_CELULAR);
        matcher = pattern.matcher(celular);

        if (matcher.matches()) celular = matcher.replaceAll("($1) $2-$3");

        return celular;
    }

    public static String retirarMascara(String texto) {
        if (isNull(texto)) {
            return null;
        }
        return texto.replaceAll("[^0-9]", "");
    }
}
